variable vpc_cidr_block {
  default="10.0.0.0/16"
}
variable private_subnet_cidr_blocks {
    default=["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24", "10.0.4.0/24"]
}
variable public_subnet_cidr_blocks {
    default=["10.0.5.0/24", "10.0.6.0/24", "10.0.7.0/24", "10.0.8.0/24", "10.0.9.0/24"]
}

# Reference: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones
# no reqd args, will return whole obj
data "aws_availability_zones" "azs" {}

# Reference: https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest?tab=resources
module "myapp-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.4.0"

  # VPC name in AWS
  name = "myapp-vpc"
  cidr = var.vpc_cidr_block
  private_subnets = var.private_subnet_cidr_blocks
  public_subnets = var.public_subnet_cidr_blocks
  # azs = data.aws_availability_zones.azs.names
  # Hard-coded 5 AZs to bypass error "UnsupportedAvailabilityZoneException: Cannot create cluster because us-east-1e does not currently have sufficient capacity"
  azs = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1f"]
  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  # tag VPC
  tags = {
    # cluster name is myapp-eks-cluster
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }

  # tag subnets
  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = 1
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}