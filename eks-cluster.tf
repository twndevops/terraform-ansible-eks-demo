module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.21.0"

  cluster_name = "myapp-eks-cluster"
  # K8s version, check w/ "kubectl version" (1.29 is NOT valid)
  cluster_version = "1.28"

  # enables EKS public API server endpoint (private API server endpoint enabled by default)
  # by default, all IPs can access public endpoint
  cluster_endpoint_public_access = true

  # Reference vpc_id output of vpc module: https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest?tab=outputs
  vpc_id = module.myapp-vpc.vpc_id

  # VPC priv subnets
  # Reference private_subnets output (list of ids) of vpc module
  subnet_ids = module.myapp-vpc.private_subnets

  # 1 EKS NodeGroup
  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3
      instance_types = ["t2.small"]
    }
  }

  tags = {
    env = "dev"
    app = "myapp"
  }
}